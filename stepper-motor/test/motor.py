#!/usr/bin/python
import RPi.GPIO as GPIO, time

GPIO.setmode(GPIO.BOARD)
GPIO.setup(16, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)

GPIO.setwarnings(False)
GPIO.output(16, True)
GPIO.output(22, True)

#iemand anders 500
p = GPIO.PWM(16, 5000)

def SpinMotor(direction, num_steps):
    GPIO.output(18, direction)
    #enable chip
    GPIO.output(22, False)
    p.ChangeFrequency(5000)

    while num_steps > 0:
        p.start(1)
        time.sleep(0.01)
        num_steps -= 1
    p.stop()

    GPIO.output(22, True)
    GPIO.cleanup()
    return True

direction_input = raw_input('Please enter O or C fro Open or Close:')
num_steps = input('Please enter the number of steps: ')
if direction_input == 'C':
    SpinMotor(False, num_steps)
else:
    SpinMotor(True, num_steps)
