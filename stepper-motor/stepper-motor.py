#!/usr/bin/python3.5

# threading.Thread
import threading
import time
import bottle
import sys, os, signal
import RPi.GPIO as GPIO

class Settings():
    currentStep = 0
    wantedStep = 0

class ServerT():
    deamon = True

    def __init__(self,settings):
        self.settings = settings

    def setWanted(self, steps):
        self.settings.wantedStep = int(steps)
        return ("OK")

    def setCurrent(self, steps):
        self.settings.currentStep = int(steps)
        return ("OK")

    def getWanted(self):
        return(str(self.settings.wantedStep))

    def getCurrent(self):
        return(str(self.settings.currentStep))


class ServerThread(threading.Thread):
    deamon = True

    def __init__(self, settings):
        self.settings = settings
        super().__init__()

    def kill(self):
        try:
            os.kill(self, signal.SIGKILL)
        except OSError: pass 

    def run(self):
        myapp = ServerT(settings = self.settings)
        bottle.route("/getWanted")(myapp.getWanted)
        bottle.route("/getCurrent")(myapp.getCurrent)
        bottle.route("/setWanted/<steps>")(myapp.setWanted)
        bottle.route("/setCurrent/<steps>")(myapp.setCurrent)
        bottle.run(host='localhost', port=9090)

class MotorManagement:
    GPIO_DIRECTION = 18
    GPIO_ENGINE = 16
    GPIO_ENABLE = 22

    GPIO_MOTORPOWER = 15
    GPIO_START = 38
    GPIO_END = 40

    CURRENT_ENGINE_STATE = None
    CURRENT_DIRECTION_STATE = None

    def __init__(self):
        print('INIT MotorManagement, set pins')
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.GPIO_ENGINE, GPIO.OUT)
        GPIO.setup(self.GPIO_DIRECTION, GPIO.OUT)
        GPIO.setup(self.GPIO_ENABLE, GPIO.OUT)
        GPIO.setup(self.GPIO_MOTORPOWER, GPIO.OUT)

        GPIO.setup(self.GPIO_START, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(self.GPIO_END, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        GPIO.setwarnings(False)
        GPIO.output(self.GPIO_ENGINE, True)
        GPIO.output(self.GPIO_ENABLE, True)
        GPIO.output(self.GPIO_MOTORPOWER, True)

    def resetMotor(self):
        pinTriggert = False
        self.turnOnEngine()
        print('RESET MOTOR, TURN LEFT UNTIL PIN TRIGGERT')
        maxTurns = 2000
        return
        while not GPIO.input(self.GPIO_START) == GPIO.HIGH:
            self.spinLeft(1)
            time.sleep(0.001)
            maxTurns = maxTurns - 1
            if maxTurns < 0:
                print('ERROR INFINITE RESET. CANT FIND START SENSOR')
                self.turnOffEngine()
                sys.exit()


        print('MOTOR IS RESET')

    def spinLeft(self, steps):
        self.setDirection('L')
        self.spinMotor(steps)
        return True

    def spinRight(self, steps):
        self.setDirection('R')
        self.spinMotor(steps)
        return True

    def spinMotor(self, steps):
        if (self.CURRENT_ENGINE_STATE != True):
            print('CANT SPIN, ENGINE OFF')
            return False

        #p = GPIO.PWM(self.GPIO_ENGINE, 5000)
        p = GPIO.PWM(self.GPIO_ENGINE, 1000)

        print('MOTORSPIN: ' + ' (' + self.CURRENT_DIRECTION_STATE + ') ' + str(steps))
        while (steps > 0):
            steps -= 1
            if GPIO.input(self.GPIO_START) == GPIO.HIGH and self.CURRENT_DIRECTION_STATE == 'L':
                #complete close, end
                print('Complete closed, stop spin')
                steps = 0
            elif GPIO.input(self.GPIO_END) == GPIO.HIGH and self.CURRENT_DIRECTION_STATE == 'R':
                #complete open, end
                print('Complete open, stop spin')
                steps = 0
            else:
                p.start(1)
                time.sleep(0.01)

        return True

    def setDirection(self, direction):
        if (self.CURRENT_DIRECTION_STATE != direction):
            self.CURRENT_DIRECTION_STATE = direction
            print('MOTORMANGEMENT: DIRECTION:' + str(direction))
            if direction == 'L':
                GPIO.output(self.GPIO_DIRECTION, True)
            else:
                GPIO.output(self.GPIO_DIRECTION, False)
        return True

    def turnOnEngine(self):
        if (self.CURRENT_ENGINE_STATE != True):
            print('MOTORMANGEMENT: TURNON')
            GPIO.output(self.GPIO_MOTORPOWER, False)
            time.sleep(.5)
            GPIO.output(self.GPIO_ENABLE, False)
            self.CURRENT_ENGINE_STATE = True

        return True

    def turnOffEngine(self):
        if (self.CURRENT_ENGINE_STATE != False):
            print('MOTORMANGEMENT: TURNOFF')
            GPIO.output(self.GPIO_MOTORPOWER, True)
            GPIO.output(self.GPIO_ENABLE, True)
            self.CURRENT_ENGINE_STATE = False

        return True

class MyMotor(threading.Thread):
    deamon = True  
    STEP_SIZE = 250

    def __init__(self, settings):
        super().__init__()
        self.settings = settings
        self.motorManagement = MotorManagement()
        self.motorManagement.resetMotor()
        self.settings.currentStep = 0
        self.kill_received = False

    def run(self):
        while not self.kill_received:
            print('Motor current/wanted:' + str(self.settings.currentStep) + "/" + str(self.settings.wantedStep) )

            if (self.settings.wantedStep != self.settings.currentStep):
                self.motorManagement.turnOnEngine()

                if (self.settings.wantedStep > self.settings.currentStep):
                    stepsToTake = self.settings.wantedStep - self.settings.currentStep
                    if (stepsToTake > self.STEP_SIZE):
                        stepsToTake = self.STEP_SIZE
                    self.motorManagement.spinRight(stepsToTake)
                    self.settings.currentStep += stepsToTake
                elif (self.settings.wantedStep < self.settings.currentStep):
                    stepsToTake = self.settings.currentStep - self.settings.wantedStep
                    if (stepsToTake > self.STEP_SIZE):
                        stepsToTake = self.STEP_SIZE
                    self.motorManagement.spinLeft(stepsToTake)
                    self.settings.currentStep -= stepsToTake

            if (self.settings.currentStep == self.settings.wantedStep):
                self.motorManagement.turnOffEngine()

            time.sleep(1)

        print('Main thread died,  turn engine off')
        self.motorManagement.turnOffEngine()


def has_live_threads(threads):
    return True in [t.isAlive() for t in threads]

if __name__ == '__main__':
    threads = []

    settings = Settings()

    t = ServerThread(settings)
    t.start() 
    threads.append(t)

    t2 = MyMotor(settings)
    t2.start()
    threads.append(t2)

    while has_live_threads(threads):
        try:
            [titem.join(1) for titem in threads
             if titem is not None and titem.isAlive()]
        except KeyboardInterrupt:
            # Ctrl-C handling and send kill to threads
            print ("Sending kill to threads...")
            for t in threads:
                t.kill_received = True
           
            #todo stop bottle webserver.. for now press 2times ctr+c
            break  

    print ("Exited")
    sys.exit()