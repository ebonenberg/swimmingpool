#! /bin/bash

printf "Set GPIO"
printf "\n"

#relay pos1
echo 17 > /sys/class/gpio/export
sudo sh -c "echo '1' >> /sys/class/gpio/gpio17/active_low"
echo high > /sys/class/gpio/gpio17/direction

#relay pos2
echo 27 > /sys/class/gpio/export
sudo sh -c "echo '1' >> /sys/class/gpio/gpio27/active_low"
echo high > /sys/class/gpio/gpio27/direction

#relay pos3
echo 22 > /sys/class/gpio/export
sudo sh -c "echo '1' >> /sys/class/gpio/gpio22/active_low"
echo high > /sys/class/gpio/gpio22/direction

#relay pos4
echo 10 > /sys/class/gpio/export
sudo sh -c "echo '1' >> /sys/class/gpio/gpio10/active_low"
echo high > /sys/class/gpio/gpio10/direction

#Push button, pump
/usr/bin/gpio export 16 in
/usr/bin/gpio edge 16 both

exit 0
